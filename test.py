# -*- coding: utf-8 -*
import os, time

# [Content] XML Footer Text
def test_hasHomePageNode():
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./log/window_dump.xml')
    f = open('./log/window_dump.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
    os.system('adb shell input tap 100 100')

############################ TODOs ############################

# 1. [Content] Side Bar Text
def test_Content_SideBarText():
    os.system('adb shell input tap 89 140')
    time.sleep(2)
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./log/test01.xml')
    f = open('./log/test01.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('查看商品分類') != -1
    assert xmlString.find('查訂單/退訂退款') != -1
    assert xmlString.find('追蹤/買過/看過清單') != -1
    assert xmlString.find('智慧標籤') != -1
    assert xmlString.find('其他') != -1
    assert xmlString.find('PChome 旅遊') != -1
    assert xmlString.find('線上手機回收') != -1
    assert xmlString.find('給24h購物APP評分') != -1
    f.close()
    time.sleep(1)
    os.system('adb shell input keyevent "KEYCODE_BACK"') #返回键
    time.sleep(3)

# 2. [Screenshot] Side Bar Text
def test_Screenshot_SideBarText():
    os.system('adb shell input tap 89 140')
    time.sleep(0.5)
    os.system('adb exec-out screencap -p > ./screenshots/SideBarText.png')
    os.system('adb shell input keyevent "KEYCODE_BACK"') #返回键
    time.sleep(3)
    
# 3. [Context] Categories
def test_Context_Categories():
    os.system('adb shell input swipe 500 1500 500 1000')
    time.sleep(0.5)
    os.system('adb shell input tap 1000 1000')
    time.sleep(0.5)
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./log/test03.xml')
    f = open('./log/test03.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('精選') != -1
    assert xmlString.find('3C') != -1
    assert xmlString.find('周邊') != -1
    assert xmlString.find('NB') != -1
    assert xmlString.find('通訊') != -1
    assert xmlString.find('數位') != -1
    assert xmlString.find('家電') != -1
    assert xmlString.find('日用') != -1
    assert xmlString.find('食品') != -1
    assert xmlString.find('生活') != -1
    assert xmlString.find('運動戶外') != -1
    assert xmlString.find('美妝') != -1
    assert xmlString.find('衣鞋包錶') != -1
    f.close()
    time.sleep(0.5)
    os.system('adb shell input tap 1000 300')
    time.sleep(0.5)
    os.system('adb shell input swipe 500 400 500 1500')
    time.sleep(3)
    
# 4. [Screenshot] Categories
def test_Screenshot_Categories():
    os.system('adb shell input swipe 500 1500 500 1000')
    time.sleep(0.5)
    os.system('adb shell input tap 1000 1000')
    time.sleep(0.5)
    os.system('adb exec-out screencap -p > ./screenshots/Categories.png')
    time.sleep(0.5)
    os.system('adb shell input tap 1000 300')
    time.sleep(0.5)
    os.system('adb shell input swipe 500 400 500 1500')
    time.sleep(3)

# 5. [Context] Categories page
def test_Context_CategoriesPage():
    os.system('adb shell input tap 324 1715')
    time.sleep(0.5)
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./log/test05.xml')
    f = open('./log/test05.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('24H購物') != -1
    assert xmlString.find('購物中心') != -1
    assert xmlString.find('3C') != -1
    assert xmlString.find('周邊') != -1
    assert xmlString.find('NB') != -1
    assert xmlString.find('通訊') != -1
    assert xmlString.find('數位') != -1
    assert xmlString.find('家電') != -1
    assert xmlString.find('日用') != -1
    assert xmlString.find('食品') != -1
    assert xmlString.find('生活') != -1
    assert xmlString.find('運動戶外') != -1
    assert xmlString.find('美妝') != -1
    assert xmlString.find('衣鞋包錶') != -1
    f.close()
    time.sleep(0.5)
    os.system('adb shell input tap 113 1715')
    time.sleep(3)

# 6. [Screenshot] Categories page
def test_Screenshot_CategoriesPage():
    os.system('adb shell input tap 324 1715')
    time.sleep(0.5)
    os.system('adb exec-out screencap -p > ./screenshots/CategoriesPage.png')
    time.sleep(0.5)
    os.system('adb shell input tap 113 1715')
    time.sleep(3)

# 7. [Behavior] Search item “switch”
def test_Behavior_SearchItem__switch__():
    os.system('adb shell input tap 666 142')
    time.sleep(0.5)
    os.system('adb shell input text "switch"')
    time.sleep(0.5)
    os.system('adb shell input keyevent "KEYCODE_ENTER"') #ENTER
    time.sleep(5)
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./log/test07.xml')
    f = open('./log/test07.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('Switch') != -1
    f.close()
    time.sleep(0.5)
    os.system('adb shell input tap 113 1715')
    time.sleep(3)

# 8. [Behavior] Follow an item and it should be add to the list
def test_Behavior_FollowAnItemAndItShouldBeAddToTheList():
    os.system('adb shell input tap 666 142')
    time.sleep(3)
    os.system('adb shell input text "switch"')
    time.sleep(3)
    os.system('adb shell input keyevent "KEYCODE_ENTER"') #ENTER
    time.sleep(3)
    os.system('adb shell input tap 600 600')
    time.sleep(3)
    os.system('adb shell input tap 100 1700')
    time.sleep(2)
    os.system('adb shell input keyevent "KEYCODE_BACK"') #返回键
    time.sleep(1)
    os.system('adb shell input keyevent "KEYCODE_BACK"') #返回键
    time.sleep(1)
    os.system('adb shell input tap 89 140')
    time.sleep(3)
    os.system('adb shell input tap 300 870')
    time.sleep(2)
    os.system('adb shell input swipe 500 500 500 1500')
    time.sleep(5)
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./log/test08.xml')
    f = open('./log/test08.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('Switch') != -1
    f.close()
    time.sleep(0.5)
    os.system('adb shell input tap 113 1715')
    time.sleep(3)

# 9. [Behavior] Navigate to the detail of item
def test_Behavior_NavigateToTheDetailOfItem():
    os.system('adb shell input tap 666 142')
    time.sleep(3)
    os.system('adb shell input text "switch"')
    time.sleep(3)
    os.system('adb shell input keyevent "KEYCODE_ENTER"') #ENTER
    time.sleep(6)
    os.system('adb shell input tap 600 600')
    time.sleep(6)
    os.system('adb shell input swipe 500 1500 500 500')
    time.sleep(3)
    os.system('adb shell input swipe 500 1500 500 500')
    time.sleep(3)
    os.system('adb shell input swipe 500 1500 500 500')
    time.sleep(5)
    os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml ./log/test09.xml')
    f = open('./log/test09.xml', 'r', encoding="utf-8")
    xmlString = f.read()
    assert xmlString.find('詳細資訊') != -1
    f.close()
    time.sleep(0.5)
    os.system('adb shell input keyevent "KEYCODE_BACK"') #返回键
    time.sleep(3)
    os.system('adb shell input keyevent "KEYCODE_BACK"') #返回键
    time.sleep(3)
    os.system('adb shell input tap 113 1715')
    time.sleep(3)

# 10. [Screenshot] Disconnetion Screen
def test_Screenshot_DisconnetionScreen():
    os.system('adb shell input swipe 1000 10 1000 1750')
    time.sleep(2)
    os.system('adb shell input tap 985 310')
    time.sleep(0.5)
    os.system('adb shell input swipe 1000 1750 1000 10')
    time.sleep(2)
    os.system('adb exec-out screencap -p > ./screenshots/DisconnetionScreen.png')
    time.sleep(2)
    os.system('adb shell input swipe 1000 10 1000 1750')
    time.sleep(2)
    os.system('adb shell input tap 985 310')
    time.sleep(0.5)
    os.system('adb shell input swipe 1000 1750 1000 10')
    time.sleep(3)
